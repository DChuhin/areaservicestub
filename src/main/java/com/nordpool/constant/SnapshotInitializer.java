package com.nordpool.constant;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nordpool.area.AreaDTO;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SnapshotInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SnapshotInitializer.class);
    private static final List<AreaDTO> initAreas = new ArrayList<>();

    private SnapshotInitializer() {
    }

    static {
        InputStream is = SnapshotInitializer.class.getClassLoader().getResourceAsStream("initSnapshot.json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        TypeReference<List<AreaDTO>> typeReference = new TypeReference<List<AreaDTO>>() {
        };
        try {
            byte[] data = IOUtils.toByteArray(is);
            initAreas.addAll(objectMapper.readValue(data, typeReference));
        } catch (IOException e) {
            LOGGER.error("Error reading init snapshot file");
        }
    }

    public static void initSnapshot(List<AreaDTO> snapshot) {
        snapshot.addAll(initAreas);
    }


}
