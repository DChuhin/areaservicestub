package com.nordpool.service;

import com.nordpool.area.AreaDTO;
import com.nordpool.constant.SnapshotInitializer;
import com.npspot.jtransitlight.JTransitLightException;
import com.npspot.jtransitlight.contract.ContractType;
import com.npspot.jtransitlight.contract.GenericContract;
import com.npspot.jtransitlight.publisher.Bus;
import com.npspot.jtransitlight.publisher.IBusControl;
import com.npspot.jtransitlight.transport.JTransitLightTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
public class AreaServiceImpl implements AreaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AreaServiceImpl.class);
    private int counter = 0;

    @Value("${rabbitmq.addr}")
    private String rabbitMqAddr;

    private List<AreaDTO> currentSnapshot = new ArrayList<>();

    private IBusControl publisher;

    private Iterator<AreaDTO> it;

    @PostConstruct
    public void init() throws URISyntaxException, JTransitLightTransportException {
        SnapshotInitializer.initSnapshot(currentSnapshot);
        initJTransitLightPublisher();
        runTimer();
    }

    @Override
    public List<AreaDTO> getFullSnapshot() {
        return currentSnapshot;
    }

    private void runTimer() {
        ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1); //should not use more then 1 for one publisher (rabbit mq channel is unsafe)
        scheduledExecutorService.scheduleAtFixedRate(this::generateAndPublishDelta, 0, 10, TimeUnit.SECONDS);
    }

    private void initJTransitLightPublisher() throws URISyntaxException, JTransitLightTransportException {
        publisher = Bus.Factory.createUsingRabbitMq(new URI(rabbitMqAddr), 5000, ContractType.REFERENCE_TYPE);
    }

    private void generateAndPublishDelta() {
        if (it == null || !it.hasNext()) {
            it = currentSnapshot.iterator();
        }
        AreaDTO delta = modifyArea(it.next());
        GenericContract.Builder<AreaDTO> builder = GenericContract.newBuilder();
        GenericContract<AreaDTO> contract = builder
                .withNamespace("Area")
                .withContractName("connection")
                .withItems(Collections.singletonList(delta))
                .withSnapshot(false)
                .build();
        try {
            publisher.publish(contract);
        } catch (JTransitLightException e) {
            LOGGER.error("Error publishing message");
        }
    }

    private AreaDTO modifyArea(AreaDTO source) {
        return AreaDTO.newBuilder()
                .withId(source.getId())
                .withCountryIsoCode(source.getCountryIsoCode())
                .withAreaType(source.getAreaType())
                .withCode(source.getCode())
                .withConnections(source.getConnections())
                .withCreatedAt(source.getCreatedAt())
                .withDeleted(source.isDeleted())
                .withEicCode(source.getEicCode())
                .withName(source.getName() + " change " + counter++)
                .withTenantId(source.getTenantId())
                .withUpdatedAt(source.getUpdatedAt())
                .build();
    }

}
