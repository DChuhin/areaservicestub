package com.nordpool.service;

import com.nordpool.area.AreaDTO;
import java.util.List;

public interface AreaService {

    List<AreaDTO> getFullSnapshot();

}
