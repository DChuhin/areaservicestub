package com.nordpool.controller;

import com.nordpool.area.AreaDTO;
import com.nordpool.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AreaController {

    private AreaService areaService;

    @Autowired
    public AreaController(AreaService areaService) {
        this.areaService = areaService;
    }

    @RequestMapping(value = "/areas/snapshot")
    public List<AreaDTO> getSnapshot() {
        return areaService.getFullSnapshot();
    }

}
