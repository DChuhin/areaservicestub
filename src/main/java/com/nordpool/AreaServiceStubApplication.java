package com.nordpool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AreaServiceStubApplication {

	public static void main(String[] args) {
		SpringApplication.run(AreaServiceStubApplication.class, args);
	}
}
